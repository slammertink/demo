package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders")
public class DemoController {

    @GetMapping(produces = (MediaType.TEXT_PLAIN_VALUE))
    public String blaat() {
        return "TESTING !@#";
    }


    @PostMapping(consumes = (MediaType.APPLICATION_JSON_VALUE))
    public ResponseEntity create(@RequestBody String order) {
        System.out.println("order");
        System.out.println(order);
        return ResponseEntity.accepted().build();
    }

}
